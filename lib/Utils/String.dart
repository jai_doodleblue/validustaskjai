class AppStrings {
  //Title
  static String myWatchList = "My Watchlist";
  static String profile = "Profile";

  //Stock Card
  static String price = "Price";
  static String dayGain = "Day gain";
  static String lastTrade = "Last trade";
  static String extendedHrs = "Extended Hrs";
  static String chance = "% Chance";

  //Profile
  static String name = "Name";
  static String email = "E-Mail";
  static String address = "Address";
  static String save = "Save";
  static String edit = "Edit";
  static String valid = "Enter Valid";

  //Tab Bar
  static String stocksTab = "Stocks";
  static String profileTab = "Profile";







}
