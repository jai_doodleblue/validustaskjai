import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  static Color bgColor = const Color(0xff1E1E3D);
  static Color cardBgColor = const Color(0xff171734);
  static Color txtTitleColor = const Color(0xffFFFFFF);
  static Color secondaryTextColor = const Color(0xffD2D2D2);
  static Color percentIncreaseColor = const Color(0xff1ACC81);
  static Color percentDecreaseColor = const Color(0xffE22716);
  static Color iconDeactiveColor = const Color(0xffA2A2AE);
  static Color iconActiveColor = const Color(0xffFFB802);
  static Color profileNameColor = const Color(0xffA2A2A2);
  static Color editBG = const Color(0xff2F3444);
}
