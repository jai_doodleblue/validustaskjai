import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:validuscoin/Utils/AppColorCodes.dart';
import 'package:validuscoin/Utils/String.dart';

import 'package:validuscoin/Ctrls/StocksCtrls.dart';
import 'package:get/get.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({
    Key? key,
  }) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  final storage = const FlutterSecureStorage();
  String name = "";
  String email = "";
  String address = "";
  final TextEditingController textCtrl = TextEditingController();
  bool toSave = false;

  @override
  void initState() {
    super.initState();
    getLocalData();
  }

  getLocalData() async {
    await storage.read(key: "name").then((value) async {
      setState(() {
        name = value!;
      });
      await storage.read(key: "email").then((value) async {
        setState(() {
          email = value!;
        });
        await storage.read(key: "address").then((value) {
          setState(() {
            address = value!;
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(AppStrings.profile.toString(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
        ),
        body: Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 24),
            child: Column(
              children: [
                Container(
                    decoration: BoxDecoration(color: AppTheme.cardBgColor),
                    child: Column(
                      children: [
                        _profiledata(AppStrings.name, name),
                        _profiledata(AppStrings.email, email),
                        _profiledata(AppStrings.address, address),
                      ],
                    ))
              ],
            )));
  }

  Widget _profiledata(String title, String data) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyle(color: AppTheme.secondaryTextColor),
              ),
              GestureDetector(
                child: Text(AppStrings.edit.toString(),
                    style: TextStyle(
                      color: AppTheme.txtTitleColor,
                      decoration: TextDecoration.underline,
                    )),
                onTap: () {
                  bootomSheet(title, data);
                },
              )
            ],
          ),
          const SizedBox(
            height: 10.0,
          ),
          Text(data, style: TextStyle(color: AppTheme.profileNameColor))
        ],
      ),
    );
  }

  Future<void> bootomSheet(String title, String data) {
    setState(() {
      textCtrl.text = data;
    });
    return showModalBottomSheet<void>(
      isScrollControlled: true,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.all(16.0),
          height: MediaQuery.of(context).size.height - 90,
          decoration: BoxDecoration(
            color: AppTheme.bgColor,
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        child: const Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          setState(() {
                            textCtrl.clear();
                          });
                        },
                      ),
                      Text(
                        "Edit $title",
                        style: TextStyle(color: Colors.white),
                      ),
                      Container()
                    ],
                  ),
                  const SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    "well send you an $title to confirm you new mail Address",
                    style: const TextStyle(color: Colors.white),
                  ),
                  const SizedBox(
                    height: 24.0,
                  ),
                  Container(
                      padding: const EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        color: AppTheme.editBG,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            title,
                            style:
                                TextStyle(color: AppTheme.secondaryTextColor),
                          ),
                          TextField(
                            controller: textCtrl,
                            keyboardType: title.toLowerCase() == "e-mail"
                                ? TextInputType.emailAddress
                                : title.toLowerCase() == "name"
                                    ? TextInputType.name
                                    : TextInputType.streetAddress,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              errorText: title.toLowerCase() == "e-mail"
                                  ? validateEmail(textCtrl.text)
                                  : title.toLowerCase() == "name"
                                      ? validateName(textCtrl.text)
                                      : validateAdd(textCtrl.text),
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                            ),
                            onChanged: (value) {},
                          )
                        ],
                      ))
                ],
              ),
              GestureDetector(
                child: Container(
                  padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                  alignment: Alignment.center,
                  color: AppTheme.iconActiveColor,
                  child: const Text('Save'),
                ),
                onTap: () async {
                  if (textCtrl.text.isNotEmpty) {
                    if (title.toLowerCase() == "name") {
                      if (validateName(textCtrl.text) == "") {
                        await storage.write(key: "name", value: textCtrl.text);
                        back();
                      } else {
                        Get.snackbar(title, "Please enter valid $title");
                      }
                    } else if (title.toLowerCase() == "e-mail") {
                      if (validateEmail(textCtrl.text) == "") {
                        await storage.write(key: "email", value: textCtrl.text);
                        back();
                      } else {
                        Get.snackbar(title, "Please enter valid $title");
                      }
                    } else if (title.toLowerCase() == "address") {
                      if (validateAdd(textCtrl.text) == "") {
                        await storage.write(
                            key: "address", value: textCtrl.text);
                        back();
                      } else {
                        Get.snackbar(title, "Please enter valid $title");
                      }
                    }
                  } else {
                    Get.snackbar(title, "Please enter  $title");
                  }
                },
              )
            ],
          ),
        );
      },
    );
  }

  void back() {
    Navigator.pop(context);
    setState(() {
      textCtrl.clear();
    });
    getLocalData();
  }

  String validateName(String value) {
    if (!(value.length > 3) && value.isNotEmpty) {
      return "Name should contain more than 3 characters";
    } else {
      return "";
    }
  }

  String validateEmail(String value) {
    if (value.isNotEmpty && value.length > 3) {
      bool emailValid =
          RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$')
              .hasMatch(value);

      return emailValid ? "" : "Please enter valid Email ";
    }
    return "Please enter valid Email ";
  }

  String validateAdd(String value) {
    if (!(value.length > 10) && value.isNotEmpty) {
      return "Address should contain more than 10 characters";
    }
    return "";
  }
}
