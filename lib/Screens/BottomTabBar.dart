
import 'package:flutter/material.dart';
import 'package:validuscoin/Utils/AppColorCodes.dart';
import 'package:validuscoin/Utils/String.dart';
import 'package:validuscoin/Utils/String.dart';
import 'package:validuscoin/Screens/Stocks/StocksList.dart';

import 'Profile/userProfile.dart';
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key,}) : super(key: key);



  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body:  _selectedIndex == 0 ? const StocksList():const UserProfile()
      ,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: AppTheme.bgColor,
        currentIndex:_selectedIndex ,
        selectedIconTheme: IconThemeData(color: AppTheme.iconActiveColor,),
        selectedItemColor: AppTheme.iconActiveColor,
        unselectedItemColor: AppTheme.iconDeactiveColor,
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold,color: AppTheme.iconActiveColor),
        unselectedIconTheme:  IconThemeData(color: AppTheme.iconDeactiveColor,),
       // selectedItemColor: AppTheme.iconDeactiveColor,
        unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
        onTap: _onItemTapped,
        items:  const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.analytics_outlined,),
            label: 'Stocks',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home_filled,),
            label: 'Profile',
          ),

        ],
      ),
    );
  }
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

}
