import 'package:flutter/material.dart';
import 'package:validuscoin/Utils/AppColorCodes.dart';
import 'package:validuscoin/Utils/String.dart';

import 'cardView.dart';
import 'package:validuscoin/Ctrls/StocksCtrls.dart';
import 'package:get/get.dart';

class StocksList extends StatefulWidget {
  const StocksList({
    Key? key,
  }) : super(key: key);

  @override
  State<StocksList> createState() => _StocksListState();
}

class _StocksListState extends State<StocksList> with TickerProviderStateMixin {
  final StocksCtrl stocCtrl = Get.put(StocksCtrl());
  late AnimationController controllerCir;

  @override
  void initState() {
    super.initState();
    controllerCir = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });
    controllerCir.repeat(reverse: true);
    stocCtrl.getSymbol();
  }

  @override
  void dispose() {
    controllerCir.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            AppStrings.myWatchList.toString(),style:  const TextStyle(fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.compare_arrows,
                color: Colors.white,
              ),
              onPressed: () {
                getXBootomSheet();
                // do something
              },
            )
          ],
        ),
        body: RefreshIndicator(
          color: AppTheme.iconActiveColor,
          onRefresh: () => stocCtrl.getSymbol(),
          child: GetX<StocksCtrl>(builder: (controller) {
            return controller.symbolItems.isNotEmpty
                ? SingleChildScrollView(
                    child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: controller.symbolItems.length,
                        itemBuilder: (context, index) => CardView(
                              stockData: controller.symbolItems[index],
                            )),
                  )
                : Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      color: AppTheme.iconActiveColor,
                      value: controllerCir.value,
                    ),
                  );
          }),
        ));
  }

  getXBootomSheet() {
    Get.bottomSheet(
      Container(
          height: 150,
          color: AppTheme.bgColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Price Sort By",
                style: TextStyle(
                    color: AppTheme.iconActiveColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ),
              const SizedBox(
                height: 20.0,
              ),
              GestureDetector(
                child: Text(
                  "High to Low",
                  style: TextStyle(
                      color: AppTheme.txtTitleColor,
                      fontWeight: FontWeight.bold),
                ),
                onTap: () {
                  stocCtrl.sorting("decending");
                  Navigator.of(context).pop();
                },
              ),
              const SizedBox(
                height: 20.0,
              ),
              GestureDetector(
                child: Text("Low to High",
                    style: TextStyle(
                        color: AppTheme.txtTitleColor,
                        fontWeight: FontWeight.bold)),
                onTap: () {
                  stocCtrl.sorting("ascending");
                  Navigator.of(context).pop();
                },
              )
            ],
          )),
    );
  }
}
