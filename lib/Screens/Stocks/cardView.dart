import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:validuscoin/Utils/AppColorCodes.dart';
import 'package:validuscoin/Utils/String.dart';

import '../../Models/StocksModel.dart';

class CardView extends StatelessWidget {
  Data stockData;

  CardView({Key? key, required this.stockData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var lstTra =
        DateTime.fromMillisecondsSinceEpoch(int.parse(stockData.lastTrade));
    var exdHrs =
        DateTime.fromMillisecondsSinceEpoch(int.parse(stockData.extendedHours));
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
      child: Container(
        decoration: BoxDecoration(
          color: AppTheme.cardBgColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  stockData.stockName,
                  style: TextStyle(
                      color: AppTheme.txtTitleColor,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppStrings.price.toString(),
                          style: TextStyle(
                            color: AppTheme.txtTitleColor,
                          ),
                        ),
                        Text(stockData.price.toString(),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(AppStrings.dayGain.toString(),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                        Text(stockData.dayGain.toString(),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(AppStrings.lastTrade.toString(),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                        Text(DateFormat('hh:mm a').format(lstTra),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(AppStrings.extendedHrs.toString(),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                        Text(DateFormat('hh:mm a').format(exdHrs),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(AppStrings.chance.toString(),
                            style: TextStyle(
                              color: AppTheme.txtTitleColor,
                            )),
                        Row(
                          children: [
                            Icon(
                              stockData.dayGain! >= 0.0
                                  ? Icons.arrow_drop_up_rounded
                                  : Icons.arrow_drop_down_rounded,
                              color: stockData.dayGain! >= 0.0
                                  ? AppTheme.percentIncreaseColor
                                  : AppTheme.percentDecreaseColor,
                            ),
                            Text(stockData.dayGain.toString(),
                                style: TextStyle(
                                  color: stockData.dayGain! >= 0.0
                                      ? AppTheme.percentIncreaseColor
                                      : AppTheme.percentDecreaseColor,
                                ))
                          ],
                        )
                      ],
                    )
                  ],
                )
              ],
            )),
      ),
    );
  }
}
