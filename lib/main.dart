import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:validuscoin/Utils/AppColorCodes.dart';
import 'package:validuscoin/Screens/BottomTabBar.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Test Founders Grotesk',
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: AppTheme.bgColor,
        backgroundColor: AppTheme.bgColor,
        appBarTheme: AppBarTheme(
            backgroundColor: AppTheme.bgColor,
            titleTextStyle:
                TextStyle(color: AppTheme.txtTitleColor, fontSize: 32),
            elevation: 0.0),
      ),
      home: MyHomePage(),
    );
  }
}
