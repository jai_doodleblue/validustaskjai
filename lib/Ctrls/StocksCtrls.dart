import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:validuscoin/Models/StocksModel.dart';

class StocksCtrl extends GetxController {
  List<Data> symbolItems = <Data>[].obs;

  Future<void> getSymbol() async {
    try {
      final response = await get(
        Uri.parse(
            "https://run.mocky.io/v3/fc3ddccf-855c-4bb6-861c-cf7896aa963e"),
      );
      if (response.statusCode == 200) {
        symbolItems.clear();
        final Map<String, dynamic> parsed = json.decode(response.body);
        final data = StocksModel.fromJson(parsed);
        for (int i = 0; i < data.data.length - 1; i++) {
          symbolItems.add(data.data[i]);
        }
      }
    } catch (e) {}
  }

  sorting(String sortBy) {
     if(sortBy == "ascending")
       {
         symbolItems.sort((a, b) => double.parse(a.price.toString()).compareTo(double.parse(b.price.toString())));
       }

    else{
       symbolItems.sort((a, b) => double.parse(b.price.toString()).compareTo(double.parse(a.price.toString())));

     }



  }
}
