class StocksModel {
  StocksModel({
    required this.success,
    required this.statusCode,
    required this.data,
  });

  late final bool success;
  late final int statusCode;
  late final List<Data> data;

  StocksModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    statusCode = json['statusCode'];
    data = List.from(json['data']).map((e) => Data.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['success'] = success;
    _data['statusCode'] = statusCode;
    _data['data'] = data.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Data {
  Data({
    required this.id,
    required this.stockName,
    required this.price,
    required this.dayGain,
    required this.lastTrade,
    required this.extendedHours,
    required this.lastPrice,
  });

  late final String id;
  late final String stockName;
  double? price;
  late final double? dayGain;
  late final String lastTrade;
  late final String extendedHours;
  late final double? lastPrice;

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    stockName = json['stockName'];
    price = json['price'] ?? 0.0;
    dayGain = json['dayGain'];
    lastTrade = json['lastTrade'];
    extendedHours = json['extendedHours'];
    lastPrice = json['lastPrice'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['stockName'] = stockName;
    _data['price'] = price;
    _data['dayGain'] = dayGain;
    _data['lastTrade'] = lastTrade;
    _data['extendedHours'] = extendedHours;
    _data['lastPrice'] = lastPrice;
    return _data;
  }
}
